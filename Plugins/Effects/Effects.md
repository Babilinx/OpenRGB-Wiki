# EffectsPlugin quick presentation

## Mainpage

[<img src="mainpage.png" height="384">](mainpage.png)

**- 1 Effects Menu and effect list<br/>**
This is the effects menu. Here you can manage your profiles, add new effects and manage them or get information about the plugin.

**- 2 Effect parameters<br/>**
This section allow you editing your current effect parameters.

**- 3 Device list<br/>**
Here you can find all your devices (including visual maps), add them to the selected effect, reverse them if the running effect allow it, or adjust the brightness independently.

**- 4 OpenRGB profile buttons<br/>**
Those are related to OpenRGB only, and therefore are not used to manage effect profile. Use the **Effect Menu** instead.

## Effect Menu

Here you can find every effect the plugin provide.<br/>
When you select one, it will be added to your effect list. You can run several effects within the same profile, and apply them to different devices.

Each effect tab provide a start/stop button, a rename button, and a delete button.

I this menu you can also access the profile managment section.<br/>
Each profile will save its effect parameters, custom name, and the selected devices.

When you save a profile, you'll be prompted to load it at OpenRGB startup, and to save effect states (if your effect was running, it will autostart when your profile is loaded).

## Effect parameters

[<img src="effectparameters.png" height="384">](effectparameters.png)

This section allow you to customise the selected effect.<br/>
Every effect has its own settings, feel free to play with it to obtain something you like.<br/>

The pattern menu allow you to save your settings for the selected effect for quick changes.

## Device List

The device list allow you to add your devices to the current effect.

[<img src="devicelist.png" height="384">](devicelist.png)

**- 1 Toggle Brightness<br/>**
This button show/hide brightness sliders for every devices.<br/>
This allow you to adress brightness on different device without needing duplicate an effect.

**- 2 Add to selected effect<br/>**
Use this button to add your devices to the current effect.<br/>
If a devices was already selected on another effect, it will be unselect to prevent running two direct mode instance on the same device (This could damage your device)

**- 3 Reverse<br/>**
If the current effect allow it, this button allow you to simply reverse your effect.
