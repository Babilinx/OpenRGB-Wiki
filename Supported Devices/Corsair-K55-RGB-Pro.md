# Corsair K55 RGB Pro

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `1B1C` | `1BA4` | Corsair K55 RGB PRO |
| `1B1C` | `1BA0` | Corsair K60 RGB PRO |
| `1B1C` | `1BAD` | Corsair K60 RGB PRO Low Profile |
| `1B1C` | `1B9B` | Corsair MM700 |
