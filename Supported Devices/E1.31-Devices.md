# E1.31 Devices

 

## Connection Type
 E1.31

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
