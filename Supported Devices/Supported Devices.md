# Supported Devices


- [Motherboards](#motherboards)
- [RAM](#ram)
- [Graphics Cards](#graphics-cards)
- [Coolers](#coolers)
- [LED Strips](#led-strips)
- [Keyboards](#keyboards)
- [Microphones](#microphones)
- [Mice](#mice)
- [Mouse Mats](#mouse-mats)
- [Headsets](#headsets)
- [Headset Stands](#headset-stands)
- [Gamepads](#gamepads)
- [Accessories](#Accessories)
- [Lights](#lights)
- [Speakers](#speakers)
- [Virtual Devices](#virtual-devices)
- [Storage](#storage)
- [Cases](#cases)
- [Other Devices](#other-devices)

## Legend


| Symbol | Meaning |
| :---: | :--- |
| ✔️ | Fully supported by OpenRGB |
| 🚨 | Support is problematic<br/>See device page for details |
| 🤖 | Feature is automatic and can not be turned off |
| ⚒️ | Partially supported by OpenRGB<br/>See device page for details |
| 🚫 | Not currently supported by OpenRGB |
| ❌ | Not applicable for this device |

## Motherboards
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[ASRock ASR RGB SMBus](ASRock-ASR-RGB-SMBus.md)|SMBus|<span title="Controller saves automatically on every update">🤖</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[ASRock Polychrome v1 SMBus](ASRock-Polychrome-v1-SMBus.md)|SMBus|<span title="Controller saves automatically on every update">🤖</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[ASRock Polychrome v2 SMBus](ASRock-Polychrome-v2-SMBus.md)|SMBus|<span title="Controller saves automatically on every update">🤖</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[ASrock Polychrome USB](ASrock-Polychrome-USB.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Asus Aura USB](Asus-Aura-USB.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[ENE SMBus Device](ENE-SMBus-Device.md)|SMBus|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Gigabyte Fusion SMBus](Gigabyte-Fusion-SMBus.md)|I2C|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Gigabyte Fusion2 SMBus](Gigabyte-Fusion2-SMBus.md)|I2C|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Gigabyte RGB Fusion 2 USB](Gigabyte-RGB-Fusion-2-USB.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Gigabyte SuperIO RGB](Gigabyte-SuperIO-RGB.md)|SuperIO|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[HP Omen 30L](HP-Omen-30L.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[MSI Mystic Light (162 Byte)](MSI-Mystic-Light-%28162-Byte%29.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[MSI Mystic Light (185 Byte)](MSI-Mystic-Light-%28185-Byte%29.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[MSI RGB](MSI-RGB.md)|SuperIO|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|

## RAM
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[Corsair Dominator Platinum](Corsair-Dominator-Platinum.md)|SMBus|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Corsair Vengeance Pro](Corsair-Vengeance-Pro.md)|SMBus|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Corsair Vengeance](Corsair-Vengeance.md)|SMBus|<span title="Controller saves automatically on every update">🤖</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Crucial RAM](Crucial-RAM.md)|SMBus|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[ENE SMBus Device](ENE-SMBus-Device.md)|SMBus|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Gigabyte Fusion2 DRAM](Gigabyte-Fusion2-DRAM.md)|I2C|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[HyperX DRAM](HyperX-DRAM.md)|I2C|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Patriot Viper Steel](Patriot-Viper-Steel.md)|I2C|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Patriot Viper](Patriot-Viper.md)|I2C|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|

## Graphics Cards
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[AMD Radeon 6000](AMD-Radeon-6000.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Asus Aura GPU](Asus-Aura-GPU.md)|SMBus|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Colorful GPU](Colorful-GPU.md)|I2C|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[ENE SMBus Device](ENE-SMBus-Device.md)|SMBus|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[EVGA GP102 GPU](EVGA-GP102-GPU.md)|I2C|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[EVGA RGB v1 GPU](EVGA-RGB-v1-GPU.md)|I2C|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[EVGA RGB v2 GPU](EVGA-RGB-v2-GPU.md)|I2C|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[EVGA RGB v3 GPU](EVGA-RGB-v3-GPU.md)|I2C|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Gainward GPU v1](Gainward-GPU-v1.md)|I2C|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Gainward GPU v2](Gainward-GPU-v2.md)|I2C|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Galax GPU](Galax-GPU.md)|I2C|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Gigabyte Fusion 2 GPU](Gigabyte-Fusion-2-GPU.md)|I2C|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Gigabyte Fusion GPU](Gigabyte-Fusion-GPU.md)|I2C|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Intel Arc A770 Limited Edition](Intel-Arc-A770-Limited-Edition.md)|USB|<span title="Not currently supported by OpenRGB">🚫</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not fully implemented by controller (See device page for details)">⚒️</span>|
|[MSI GPU](MSI-GPU.md)|I2C|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[NVIDIA Illumination](NVIDIA-Illumination.md)|PCI|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[PNY GPU](PNY-GPU.md)|I2C|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Razer](Razer.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Sapphire Nitro Glow v1](Sapphire-Nitro-Glow-v1.md)|I2C|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Sapphire Nitro Glow v3](Sapphire-Nitro-Glow-v3.md)|I2C|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|

## Coolers
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[AMD Wraith Prism](AMD-Wraith-Prism.md)|USB|<span title="Not currently supported by OpenRGB">🚫</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not fully implemented by controller (See device page for details)">⚒️</span>|
|[Aorus ATC800](Aorus-ATC800.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Asus Aura Ryuo AIO ](Asus-Aura-Ryuo-AIO-.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Asus ROG Strix Liquid Cooler](Asus-ROG-Strix-Liquid-Cooler.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Corsair Commander Core](Corsair-Commander-Core.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Corsair Hydro Platinum](Corsair-Hydro-Platinum.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Corsair Hydro](Corsair-Hydro.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[FanBus](FanBus.md)|Serial|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Lian Li Uni Hub](Lian-Li-Uni-Hub.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is problematic (See device page for details)">🚨</span>|<span title="Hardware effects are supported">✔️</span>|
|[NZXT Kraken](NZXT-Kraken.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Thermaltake Riing Quad](Thermaltake-Riing-Quad.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Thermaltake Riing](Thermaltake-Riing.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|

## LED Strips
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[Asus AURA Core](Asus-AURA-Core.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is problematic (See device page for details)">🚨</span>|<span title="Hardware effects are supported">✔️</span>|
|[Asus Aura Monitor](Asus-Aura-Monitor.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Blinky Tape](Blinky-Tape.md)|Serial|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Coolermaster ARGB A1](Coolermaster-ARGB-A1.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Coolermaster ARGB](Coolermaster-ARGB.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Coolermaster RGB](Coolermaster-RGB.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Coolermaster Small ARGB](Coolermaster-Small-ARGB.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Corsair Lighting Node](Corsair-Lighting-Node.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Dummy](Dummy.md)|File Stream|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[E1.31 Devices](E1.31-Devices.md)|E1.31|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[EK Loop Connect](EK-Loop-Connect.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Lian Li Strimer L Connect](Lian-Li-Strimer-L-Connect.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is problematic (See device page for details)">🚨</span>|<span title="Hardware effects are supported">✔️</span>|
|[MSI Optix](MSI-Optix.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[NZXT Hue+](NZXT-Hue+.md)|Serial|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[NZXT Hue2](NZXT-Hue2.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Razer ARGB](Razer-ARGB.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[SRGBmods Raspberry Pi Pico LED Controller](SRGBmods-Raspberry-Pi-Pico-LED-Controller.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Serial LED Strip](Serial-LED-Strip.md)|Serial|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[ThingM Blink](ThingM-Blink.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Zalmna Z Sync](Zalmna-Z-Sync.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|

## Keyboards
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[ASUS TUF Keyboard (Faustus)](ASUS-TUF-Keyboard-%28Faustus%29.md)|File Stream|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Alienware AW510 Keyboard](Alienware-AW510-Keyboard.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Alienware](Alienware.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Anne Pro 2](Anne-Pro-2.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Asus AURA Core](Asus-AURA-Core.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is problematic (See device page for details)">🚨</span>|<span title="Hardware effects are supported">✔️</span>|
|[Asus Aura Keyboard](Asus-Aura-Keyboard.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Asus Aura TUF Keyboard](Asus-Aura-TUF-Keyboard.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Asus Cerberus Mech Keyboard](Asus-Cerberus-Mech-Keyboard.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Asus TUF Laptop](Asus-TUF-Laptop.md)|WMI|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Coolermaster Masterkeys Keyboards](Coolermaster-Masterkeys-Keyboards.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Corsair K100 Keyboard](Corsair-K100-Keyboard.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Corsair K55 RGB Pro XT](Corsair-K55-RGB-Pro-XT.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Corsair K55 RGB Pro](Corsair-K55-RGB-Pro.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Corsair K65 Mini](Corsair-K65-Mini.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Corsair K95 Platinum XT Keyboard](Corsair-K95-Platinum-XT-Keyboard.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Corsair Peripheral](Corsair-Peripheral.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not fully implemented by controller (See device page for details)">⚒️</span>|
|[Corsair Wireless Peripheral](Corsair-Wireless-Peripheral.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Cougar 700K Evo Keyboard](Cougar-700K-Evo-Keyboard.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Dark Project Keyboard](Dark-Project-Keyboard.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Das Keyboard](Das-Keyboard.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Ducky Keyboard](Ducky-Keyboard.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Dygma Raise Keyboard](Dygma-Raise-Keyboard.md)|Serial|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[EVGA USB Keyboard](EVGA-USB-Keyboard.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[EVision Keyboard](EVision-Keyboard.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[GaiZhongGai Keyboard](GaiZhongGai-Keyboard.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Genesis Thor 300](Genesis-Thor-300.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Not supported by controller">🚨</span>|<span title="Hardware effects are supported">✔️</span>|
|[HyperX Alloy Elite 2](HyperX-Alloy-Elite-2.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[HyperX Alloy Elite](HyperX-Alloy-Elite.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[HyperX Alloy FPS](HyperX-Alloy-FPS.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[HyperX Alloy Origins 60](HyperX-Alloy-Origins-60.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[HyperX Alloy Origins Core](HyperX-Alloy-Origins-Core.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[HyperX Alloy Origins](HyperX-Alloy-Origins.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Keychron Keyboard](Keychron-Keyboard.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Lenovo USB](Lenovo-USB.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Logitech G Pro](Logitech-G-Pro.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Logitech G213](Logitech-G213.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Logitech G810](Logitech-G810.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Logitech G815](Logitech-G815.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Logitech G910](Logitech-G910.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Logitech G915](Logitech-G915.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Logitech Lightspeed](Logitech-Lightspeed.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[MSI 3 Zone Keyboard](MSI-3-Zone-Keyboard.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[MSI GL66 Mystic Light Keyboard (64 Byte)](MSI-GL66-Mystic-Light-Keyboard-%2864-Byte%29.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects"></span>|<span title="Hardware effects are supported">✔️</span>|
|[MSI Vigor GK30](MSI-Vigor-GK30.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Openrazer Windows](Openrazer-Windows.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Openrazer](Openrazer.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Quantum Mechanical Keyboard (QMK)](Quantum-Mechanical-Keyboard-%28QMK%29.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Razer](Razer.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Roccat Horde Aimo](Roccat-Horde-Aimo.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Roccat Vulcan Aimo](Roccat-Vulcan-Aimo.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Sinowealth Keyboard 16](Sinowealth-Keyboard-16.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Sinowealth Keyboard](Sinowealth-Keyboard.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Steel Series APEX](Steel-Series-APEX.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Steel Series Apex (Old)](Steel-Series-Apex-%28Old%29.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Steel Series Apex Tri Zone Keyboards](Steel-Series-Apex-Tri-Zone-Keyboards.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Thermaltake PoseidonZ](Thermaltake-PoseidonZ.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Wooting Keyboards](Wooting-Keyboards.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Zet Blade Optical](Zet-Blade-Optical.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|

## Microphones
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[Razer](Razer.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|

## Mice
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[Asus Aura Mouse](Asus-Aura-Mouse.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Asus Aura Strix Evolve](Asus-Aura-Strix-Evolve.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Asus Strix Claw](Asus-Strix-Claw.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are not fully implemented by controller (See device page for details)">⚒️</span>|
|[BloodyMouse](BloodyMouse.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Coolermaster Master Mouse](Coolermaster-Master-Mouse.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Coolermaster Master Mouse](Coolermaster-Master-Mouse.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Corsair Peripheral](Corsair-Peripheral.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not fully implemented by controller (See device page for details)">⚒️</span>|
|[Cougar Revenger ST](Cougar-Revenger-ST.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Holtek A070](Holtek-A070.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Holtek A1FA](Holtek-A1FA.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[HyperX Pulsefire Dart](HyperX-Pulsefire-Dart.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[HyperX Pulsefire FPS](HyperX-Pulsefire-FPS.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[HyperX Pulsefire Haste](HyperX-Pulsefire-Haste.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[HyperX Pulsefire Raid](HyperX-Pulsefire-Raid.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[HyperX Pulsefire Surge](HyperX-Pulsefire-Surge.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Lexip Mouse](Lexip-Mouse.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Logitech G203L](Logitech-G203L.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Logitech Lightspeed](Logitech-Lightspeed.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Logitech Lightsync Mouse (1 Zone)](Logitech-Lightsync-Mouse-%281-Zone%29.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Logitech Lightsync Mouse](Logitech-Lightsync-Mouse.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[N5312A mouse](N5312A-mouse.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Openrazer Windows](Openrazer-Windows.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Openrazer](Openrazer.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Razer](Razer.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Redragon Mice](Redragon-Mice.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Roccat Burst Mouse](Roccat-Burst-Mouse.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Roccat Kone Aimo](Roccat-Kone-Aimo.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Sinowealth 1007 Mouse](Sinowealth-1007-Mouse.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Sinowealth Mice](Sinowealth-Mice.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Steel Series Rival 3](Steel-Series-Rival-3.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Steel Series Rival](Steel-Series-Rival.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Steel Series Sensei](Steel-Series-Sensei.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Tecknet Mouse](Tecknet-Mouse.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Trust GXT 114](Trust-GXT-114.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Trust GXT 180](Trust-GXT-180.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[ZET Edge Air Pro](ZET-Edge-Air-Pro.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|

## Mouse Mats
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[Asus Aura Mousemat](Asus-Aura-Mousemat.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Coolermaster Mouse Pad](Coolermaster-Mouse-Pad.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Corsair Peripheral](Corsair-Peripheral.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not fully implemented by controller (See device page for details)">⚒️</span>|
|[HyperX Mousemat](HyperX-Mousemat.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Logitech Lightspeed](Logitech-Lightspeed.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Logitech Powerplay Mat](Logitech-Powerplay-Mat.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Openrazer Windows](Openrazer-Windows.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Openrazer](Openrazer.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Razer](Razer.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Steel Series QCK Mat](Steel-Series-QCK-Mat.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|

## Headsets
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[Creative Sound BlasterX G6](Creative-Sound-BlasterX-G6.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Logitech G933](Logitech-G933.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Logitech Lightspeed](Logitech-Lightspeed.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Razer Kraken](Razer-Kraken.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Steel Series Siberia](Steel-Series-Siberia.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Steelseries Arctis 5](Steelseries-Arctis-5.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|

## Headset Stands
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[Asus Aura Headset Stand](Asus-Aura-Headset-Stand.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Corsair Peripheral](Corsair-Peripheral.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not fully implemented by controller (See device page for details)">⚒️</span>|
|[Openrazer Windows](Openrazer-Windows.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Openrazer](Openrazer.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Razer](Razer.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|

## Gamepads
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[Logitech X56](Logitech-X56.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Sony Dual Sense controller](Sony-Dual-Sense-controller.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Sony Dual Shock 4 controller](Sony-Dual-Shock-4-controller.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|

## Accessories
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[Razer](Razer.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|

## Lights
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[Espurna](Espurna.md)|TCP|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[LIFX Globes](LIFX-Globes.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Nanoleaf](Nanoleaf.md)|Network|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Philips Hue Entertainment](Philips-Hue-Entertainment.md)|Network|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Philips Hue](Philips-Hue.md)|Network|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Philips Wiz](Philips-Wiz.md)|Network|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Yeelight](Yeelight.md)|Network|<span title="Not supported by controller">❌</span>|<span title="Direct control is problematic (See device page for details)">🚨</span>|<span title="Hardware effects are supported">✔️</span>|

## Speakers
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[Logitech G560](Logitech-G560.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are not supported by controller">❌</span>|

## Virtual Devices
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |

## Storage
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[ENE SMBus Device](ENE-SMBus-Device.md)|SMBus|<span title="Saving is supported by this controller">✔️</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|

## Cases
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[NVidia ESA](NVidia-ESA.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
|[Razer](Razer.md)|USB|<span title="Controller saves automatically on every update">🤖</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|

## Other Devices
| Controller Name | Connection | Save to Flash | Direct | Hardware Effects |
| :--- | :---: | :---: | :---: | :---: |
|[AsusAuraCoreLaptop](AsusAuraCoreLaptop.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is supported for Software Effects">✔️</span>|<span title="Hardware effects are supported">✔️</span>|
|[Debug](Debug.md)|I2C|<span title="Not supported by controller">❌</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are not supported by controller">❌</span>|
|[Lian Li Uni Hub AL](Lian-Li-Uni-Hub-AL.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is problematic (See device page for details)">🚨</span>|<span title="Hardware effects are supported">✔️</span>|
|[Lian Li Uni Hub](Lian-Li-Uni-Hub.md)|USB|<span title="Not supported by controller">❌</span>|<span title="Direct control is problematic (See device page for details)">🚨</span>|<span title="Hardware effects are supported">✔️</span>|
|[Sinowealth Glorious Model O Wireless](Sinowealth-Glorious-Model-O-Wireless.md)|USB|<span title="Saving is supported by this controller">✔️</span>|<span title="Not supported by controller">❌</span>|<span title="Hardware effects are supported">✔️</span>|
