# HyperX Pulsefire FPS

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are not supported by controller

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0951` | `16D7` | HyperX Pulsefire FPS Pro |
| `0951` | `16DE` | HyperX Pulsefire Core |
