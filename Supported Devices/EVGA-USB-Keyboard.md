# EVGA USB Keyboard

 The EVGA USB keyboard controller currently supports
the Z15 (both ISO & ANSI) as well as the Z20 ANSI keyboards

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `3842` | `260E` | EVGA Z15 Keyboard |
| `3842` | `2608` | EVGA Z15 Keyboard |
| `3842` | `260A` | EVGA Z20 Keyboard |
