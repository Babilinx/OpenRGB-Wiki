# HyperX Alloy Elite

 

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0951` | `16BE` | HyperX Alloy Elite RGB |
| `0951` | `1711` | HyperX Alloy Elite 2 |
| `03F0` | `058F` | HyperX Alloy Elite 2 (HP) |
