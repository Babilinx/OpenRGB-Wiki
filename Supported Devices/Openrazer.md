# Openrazer

 The Openrazer controller has been deprecated in favour of
the in built Razer controller.

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `` | `` |  |
