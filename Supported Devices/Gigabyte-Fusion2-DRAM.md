# Gigabyte Fusion2 DRAM

 

## Connection Type
 I2C

## Saving
 Not supported by controller

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `:` | `:` |  |
