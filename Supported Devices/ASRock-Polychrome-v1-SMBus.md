# ASRock Polychrome v1 SMBus

 ASRock Polychrome v1 controllers will save with each update.
Per ARGB LED support is not possible with these devices.
ARGB size  and color order is set using the `ARGB Header Config mode`
`Right` = GRB mode that is needed for WS2812B ARGB devices and `Left` = RGB used for WS2811 strips
The modes speed slider will set the size of the header
Spectrum Cycles uses the RGB values to set the individual color brightness.

## Connection Type
 SMBus

## Saving
 Controller saves automatically on every update

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `:` | `:` |  |
