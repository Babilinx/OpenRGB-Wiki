# Yeelight

 

## Connection Type
 Network

## Saving
 Not supported by controller

## Direct Mode
 Direct control is problematic (See device page for details)

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
