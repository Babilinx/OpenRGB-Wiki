# Asus Aura USB

 The Asus Aura USB controller applies to most AMD and
Intel mainboards from the x470 and z390 chipsets onwards.

## Connection Type
 USB

## Saving
 Not supported by controller

## Direct Mode
 Direct control is supported for Software Effects

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0B05` | `1889` | ASUS ROG AURA Terminal |
| `0B05` | `1867` | ASUS Aura Addressable |
| `0B05` | `1872` | ASUS Aura Addressable |
| `0B05` | `18A3` | ASUS Aura Addressable |
| `0B05` | `18A5` | ASUS Aura Addressable |
| `0B05` | `18F3` | ASUS Aura Motherboard |
| `0B05` | `1939` | ASUS Aura Motherboard |
| `0B05` | `19AF` | ASUS Aura Motherboard |
