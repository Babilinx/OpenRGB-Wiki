# EVision Keyboard

 

## Connection Type
 USB

## Saving
 Controller saves automatically on every update

## Direct Mode
 Not supported by controller

## Hardware Effects
 Hardware effects are supported

## Device List

| Vendor ID | Product ID | Device Name |
| :---: | :---: | :--- |
| `0C45` | `5204` | EVision Keyboard 0C45:5204 |
| `0C45` | `5104` | EVision Keyboard 0C45:5104 |
| `0C45` | `5004` | EVision Keyboard 0C45:5004 |
| `0C45` | `652F` | EVision Keyboard 0C45:652F |
| `0C45` | `8520` | EVision Keyboard 0C45:8520 |
| `320F` | `502A` | EVision Keyboard 320F:502A |
| `0C45` | `7698` | EVision Keyboard 0C45:7698 |
